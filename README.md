# Agenda

# Conception : 

### User Stories : 

* En tant que utilisateur je veux pouvoir m'inscrire à l'application . 

=> estimé à 5

* En tant que utilisateur je veux pouvoir me connecter l'application .

=> estimé à 8

* En tant que utilisateur connecté je veux pouvoir créer un évènement.

=> estimé à 5

* En tant que utilisateur connecté je veux pouvoir modifier les évènements que j'ai créé.

=> estimé à 5

* En tant que utilisateur connecté je veux pouvoir supprimer les évènements que j'ai créé.

=> estimé à 3

* En tant que utilisateur connecté je veux pouvoir consulter tous les évènements de mon agenda.

=> estimé à 8

* En tant que utilisateur connecté je veux pouvoir commenter un évènement .

=> estimé à 8

* En tant que utilisateur connecté je veux pouvoir supprimer un commentaire à moi.

=> estimé à 2

* En tant que utilisateur connecté je veux pouvoir modifier un commentaire à moi.

=> estimé à 2

* En tant que utilisateur je veux pouvoir consulter mon profil.

=> estimé à 5

* En tant que utilisateur je veux pouvoir modifier mon profil.

=> estimé à 8

### Tests d'acceptance :

| User Stories | Tests d'acceptance |
| ------ | ------ |
| En tant que utilisateur je veux pouvoir me connecter l'application .  | Avoir un compte|
| En tant que utilisateur connecté je veux pouvoir créer un évènement. | Etre connecté |
| En tant que utilisateur connecté je veux pouvoir modifier les évènements que j'ai créé. | Etre connecté & avoir un évènement |
| En tant que utilisateur connecté je veux pouvoir supprimer les évènements que j'ai créé. | Etre connecté & avoir un évènement |
| En tant que utilisateur connecté je veux pouvoir commenter un évènement. | Etre connecté & être lié à l'évènement |
| En tant que utilisateur connecté je veux pouvoir supprimer un commentaire | Etre connecté & être lié à l'évènement & êêtre l'auteur du commentaire |
| En tant que utilisateur je veux pouvoir consulter/modifier mon profil. | Etre connecté |




### Maquettes fonctionnelles :
