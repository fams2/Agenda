const express = require('express');
const app = express();
const bodyParser = require("body-parser");

import myDB from './db';
// require("./routes/index")(app);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

myDB.authenticate().then(() => {
 console.log("........CONNECTED");
  
}).catch((err) => {
  console.log("ERROR..../", err);
  
});

app.get('/', (req, res) => {
  res.send("hello word");
});

app.post('/register', function (req, res) {
  res.end();
});


export default app;
