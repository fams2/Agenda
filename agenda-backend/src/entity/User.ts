module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define("user", {
    id: {
      type: Sequelize.NUMBER
    },
    pseudo: {
      type: Sequelize.STRING
    },
    password: {
      type: Sequelize.STRING
    },
    roles: {
      type: Sequelize.STRING
    }
  });

  return User;
};
