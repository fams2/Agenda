module.exports = app => {
    const users = require("../controller/UserController");
  
    var router = require("express").Router();
  
    // // register
    // router.post("/register", users.create);
  
    // Retrieve all users
    router.get("/users", users.findAll);
  
    // Retrieve all published users
    router.get("/published", users.findAllPublished);
  
    // Retrieve a single users with id
    router.get("/:id", users.findOne);
  
    // Update a users with id
    router.put("/:id", users.update);
  
    // Delete a users with id
    router.delete("/:id", users.delete);
  
    // Delete all users
    router.delete("/", users.deleteAll);
  
    app.use('/api/users', router);
  };