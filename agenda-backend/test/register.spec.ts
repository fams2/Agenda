import { expect } from "chai";
import * as chai from 'chai';
import * as request from 'supertest';

import app from "../src/app";

describe("Server!", () => {
 
  it("adds one movie to api /register", async (  ) => {
    
    let objectRegister = { pseudo: 'TEST', password: 'TEST' };
    try{
      const response = await request(app)
      .post("/register")
      .send(objectRegister)
      .set('Content-Type', 'application/json')
      expect(response.status).to.be.equal(200);
    } catch(err){
      console.log(err);
      
    }
  });

  it("add one movie is null /register", async (  ) => {
    
    try{
      const response = await request(app)
      .post("/register")
      .send(null)
      .set('Content-Type', 'application/json')
      expect(response.status).to.be.equal(400);
    } catch(err){
      console.log(err);
      
    }
  });

  it("add one movie whit no data", async (  ) => {
    
    let objectRegister = { };
    try{
      const response = await request(app)
      .post("/register")
      .send(objectRegister)
      .set('Content-Type', 'application/json')
      expect(response.status).to.be.equal(400);
    } catch(err){
      console.log(err);
    }
  });
});
